export function newGame () {
	return {
		type: 'newGame'
	};
}

export function addPiece (title, player) {
	return {
		type: 'addPiece',
		player,
		title
	};
}
