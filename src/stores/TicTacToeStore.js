import { createStore } from 'redux';
import TicTacToeReducers from '../reducers/TicTacToeReducers';

export default createStore(TicTacToeReducers);
