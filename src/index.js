import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import TicTacToeStore from './stores/TicTacToeStore';
import TicTacToe from './components/TicTacToe';

const el = document.getElementById('tictactoe');

render(
	<Provider store={TicTacToeStore}>
		<TicTacToe />
	</Provider>,
	el
);
