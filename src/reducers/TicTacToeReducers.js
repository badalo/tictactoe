import Immutable, { List, Map } from 'immutable';

const INITIAL_PLAYER = 'x';
const DEFAULT_STATS = Map({
	wins: 0,
	draws: 0,
	losses: 0
});
const WIN_SEQUENCES = List([
	List.of(1, 2, 3),
	List.of(4, 5, 6),
	List.of(7, 8, 9),
	List.of(1, 4, 7),
	List.of(2, 5, 8),
	List.of(3, 6, 9),
	List.of(1, 5, 9),
	List.of(3, 5, 7)
]);
const defaultState = {
	game: List(['x', 'o', 'x', 'o', 'x', 'o', 'x', 'o', 'x']),
	result: false,
	player: null,
	winner: null,
	stats: Map({ x: DEFAULT_STATS, o: DEFAULT_STATS })
};

export default (state = Map(defaultState), action) => {
	switch (action.type) {
		// initialize game
		case 'newGame':
			return newGame(state);
		// Update game pieces
		case 'addPiece':
			return addPiece(state, action.title, action.player);
		// No action
		default:
			return state;
	}
}

function newGame (state) {
	return state
		.set('game', List(Array(9).fill('')))
		.set('result', false)
		.set('player', INITIAL_PLAYER)
		.set('winner', null)
		.set('winnerSequence', null);
}

function checkWinner (game, player) {
	return WIN_SEQUENCES.filter((seq) => seq.every((key) => {
		const value = game.get(key - 1);

		return value && value === player;
	}));
}

function hasMaxPlays (state) {
	return state.get('game').every((value) => !!value);
}

function updateResultsDraws (state) {
	return state.get('stats').map((player) => player.set('draws', player.get('draws') + 1));
}

function updateResultsWins (state, winner) {
	return state.get('stats').map((player, key) => {
		const isPlayerWinner = key === winner;
		const playerWins = player.get('wins');
		const playerLosses = player.get('losses');

		return player
			.set('wins', isPlayerWinner ? playerWins + 1 : playerWins)
			.set('losses', !isPlayerWinner ? playerLosses + 1 : playerLosses);
	});
}

function updateWin (state, player, winnerSequence) {
	return state
		.set('winner', player)
		.set('winnerSequence', winnerSequence)
		.set('result', true)
		.set('stats', updateResultsWins(state, player));
}

function updateDraw (state) {
	return state
		.set('result', true)
		.set('stats', updateResultsDraws(state));
}

function addPiece (state, title, player) {
	// clone state and update game play and number of plays
	const newState = state
		.setIn(['game', title], player)
		.set('player', player === 'x' ? 'o': 'x');
	const winnerSequence = checkWinner(newState.get('game'), player);

	// check if there is a winner
	if (!winnerSequence.isEmpty()) {
		return updateWin(newState, player, winnerSequence.first());
	}

	// check if is a draw
	if (hasMaxPlays(newState)) {
		return updateDraw(newState);
	}

	return newState;
}
