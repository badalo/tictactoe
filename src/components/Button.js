import React from 'react';
import styles from './Button.css';

export default ({ label, onClick }) => (
	<button
		className={styles.button}
		onClick={onClick}
		>
		{label}
	</button>
)

// const styles = {
// 	button: {
// 		marginLeft: 10,
// 		marginRight: 10,
// 		fontSize: 14
// 	}
// }
