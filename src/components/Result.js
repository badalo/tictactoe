import React from 'react';
import { newGame } from '../actions/TicTacToeActions';
import Button from './Button';
import styles from './Result.css';

export default ({ state, dispatch }) => {
	const result = state.get('winner') ?
		<div>Player {state.get('winner').toUpperCase()} is the winner</div> :
		<div>That was a close one...</div>;

	return (
		<div className={styles.displayer}>
			{result}
			<Button
				label={'New game'}
				onClick={() => dispatch(newGame())}
				/>
		</div>
	);
}
