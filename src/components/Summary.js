import React from 'react';
import { connect } from 'react-redux';
import SummaryPlayer from './SummaryPlayer';
import Result from './Result';
import styles from './Summary.css';

const Summary = ({ state, dispatch }) => {
	const winner = !state.get('result') ?
		null :
		<Result state={state} dispatch={dispatch} />;
	const playing = state.get('player');
	const players = state.get('stats').map((player, key) => (
		<SummaryPlayer
			key={key}
			name={key}
			playing={key === playing && !winner}
			{...player.toJS()}
			/>
	));

	return (
		<div className={styles.summary}>
			<div className={styles.marquee}>
				{winner}
			</div>
			<div className={styles.players}>
				{players.toArray()}
			</div>
		</div>
	);
}

function mapStateToProps (state) {
	return { state };
}

export default connect(mapStateToProps)(Summary);
