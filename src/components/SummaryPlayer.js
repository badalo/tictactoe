import React from 'react';
import SummaryPlayerStat from './SummaryPlayerStat';
import styles from './SummaryPlayer.css';

export default ({ name, playing, wins, draws, losses }) => {
	const playerClasses = playing ? `${styles.name} ${styles.playing}` : styles.name;

	return (
		<div className={styles.player}>
			<h4 className={playerClasses}>{`Player '${name.toUpperCase()}'`}</h4>
			<div className={styles.stats}>
				<SummaryPlayerStat key={'Wins'} label={'Wins'} value={wins} />
				<SummaryPlayerStat key={'Draws'} label={'Draws'} value={draws} />
				<SummaryPlayerStat key={'Losses'} label={'Losses'} value={losses} />
			</div>
		</div>
	);
}
