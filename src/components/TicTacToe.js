import React from 'react';
import { connect } from 'react-redux';
import styles from './TicTacToe.css';

import Board from './Board';
import Summary from './Summary';
import Teaser from './Teaser';

const TicTacToe = ({ state, dispatch }) => {
	const teaser = state.get('player') ? null : <Teaser />;
	const summary = !state.get('player') ? null : <Summary />;

	return (
		<div className={styles.container}>
			<Board />
			{teaser}
			{summary}
		</div>
	);
}

function mapStateToProps (state) {
	return { state };
}

export default connect(mapStateToProps)(TicTacToe);
