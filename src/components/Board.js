import React from 'react';
import { List } from 'immutable';
import { connect } from 'react-redux';
import Title from './Title';
import { addPiece } from '../actions/TicTacToeActions';
import styles from './Board.css';

const Board = ({ state, dispatch }) => {
	const hasWinner = state.get('winner', false) || state.get('result', false);
	const winnerSequence = state.get('winnerSequence') || List();
	const titles = state.get('game').map((value, index) => {
		const isInWinnerSequence = winnerSequence.keyOf(index + 1);

		return (
			<Title
				key={index}
				highlight={isInWinnerSequence || isInWinnerSequence === 0}
				played={value}
				onClick={value || hasWinner ? null : () => dispatch(addPiece(index, state.get('player')))}
				/>
		)}
	);

	return (
		<div className={styles.container}>
			<div className={styles.board}>
				{titles}
			</div>
		</div>
	);
}

function mapStateToProps (state) {
	return { state };
}

export default connect(mapStateToProps)(Board);
