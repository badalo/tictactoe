import React from 'react';
import styles from './SummaryPlayerStat.css';

export default ({ label, value }) => (
	<div className={styles.stat}>
		<div className={styles.label}>{label}</div>
		<div className={styles.value}>{value}</div>
	</div>
)
