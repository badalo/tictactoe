import React from 'react';
import { connect } from 'react-redux';
import { newGame } from '../actions/TicTacToeActions';
import playGames from '../assets/playgames.mp3';
import Button from './Button';
import styles from './Teaser.css';

const Teaser = ({ state, dispatch }) => {
	return (
		<div className={styles.container}>
			<audio src={playGames} autoPlay />
			<Button
				label={'Start playing'}
				onClick={() => dispatch(newGame())}
				/>
		</div>
	);
}

function mapStateToProps (state) {
	return { state };
}

export default connect(mapStateToProps)(Teaser);
