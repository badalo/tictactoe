import React from 'react';
import styles from './Title.css';

export default ({ played, highlight, onClick }) => {
	const titleStyles = !highlight && highlight !== 0 ?
		styles.title :
		`${styles.title} ${styles.highlight}`;

	return (
		<div
			className={titleStyles}
			onClick={onClick}
			>
			{played}
		</div>
	)
}
