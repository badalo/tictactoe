const path = require('path')
const webpack = require('webpack')

module.exports = {
  entry: {
		main: './src/index.js'
	},
  output: {
    path: path.join(__dirname, 'build', 'js'),
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
	devtool: 'source-map',
  module: {
    loaders: [
			{
	      test: /.jsx?$/,
	      loader: 'babel-loader',
	      exclude: /node_modules/,
	      query: {
	          presets: ['es2015', 'react']
	      }
    	},
			{
				test: /\.css$/,
				loaders: [
					'style-loader',
					'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
				]
			},
			{
				test: /\.mp3?$/,
				loader: 'file-loader'
			}
		]
  }
}
