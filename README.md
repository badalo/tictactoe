# Installation
On a Terminal window run:

* `mkdir <path/to/folder>`

* `git clone https://badalo@bitbucket.org/badalo/tictactoe.git <path/to/folder>`

* `cd <path/to/folder>`

* `npm i`

_NOTE: You may need super user permissions_

# Start server
On a Terminal window run

* `npm start`

On a browse window open

* `http://localhost:8080/`

# Missing tests code notes
Although, usually, I start the development of my apps by creating the tests, for this one, and due to time constraints, these have been left out. If more time can be assigned, I would be happy to develop them, as i think they play a huge role when developing.

My initial thoughts were to use the following libraries:

* `mocha` => Test framework
* `chai` => Assertion library
* `chai-immutable` => Assertions for Immutable library

Structure wise, the test would be divided into two areas:

* `reducer_tests.js` => Where all the reducers actions and functions would be tested
* `components/<component>_tests.js` => File that would include each component's tests